Redis:

findByDeploymentId 0.14 ms
saveMediaStream    0.18 ms
findMediaStream    0.10 ms
deleteMediaStream  0.10 ms

Threads #1  throughput 2413/sec
Threads #2  throughput 3609/sec
Threads #4  throughput 3278/sec
Threads #8  throughput 2486/sec
Threads #16 throughput 1348/sec

Mongo:

findByDeploymentId  0.13 ms
saveMediaStream     0.12 ms
findMediaStream     0.13 ms
deleteMediaStream   0.10 ms

Threads #1  throughput 2562/sec
Threads #2  throughput 3503/sec
Threads #4  throughput 3121/sec
Threads #8  throughput 2335/sec
Threads #16 throughput 1197/sec

Cache:

findByDeploymentId  0.000149 ms
saveMediaStream     0.000240 ms
findMediaStream     0.000091 ms
deleteMediaStream   0.000200 ms

Threads 1  throughput 4020877/sec
Threads 2  throughput 2397117/sec
Threads 4  throughput 1169044/sec
Threads 8  throughput 497197/sec
Threads 16 throughput 254070/sec

