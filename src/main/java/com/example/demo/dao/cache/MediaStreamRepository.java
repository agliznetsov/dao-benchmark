package com.example.demo.dao.cache;

import com.example.demo.model.MediaStream;
import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MediaStreamRepository {
    Cache<String, MediaStream> cache;

    public MediaStreamRepository() {
        CacheManager cacheManager = CacheManagerBuilder.newCacheManagerBuilder().build();
        cacheManager.init();
        cache = cacheManager.createCache("cache", CacheConfigurationBuilder.newCacheConfigurationBuilder(String.class, MediaStream.class, ResourcePoolsBuilder.heap(10_000)));
    }

    public List<MediaStream> findByDeploymentId(String deploymentId) {
        List<MediaStream> list = new ArrayList<>();
        cache.forEach(it -> {
            if (deploymentId.equals(it.getValue().getDeploymentId())) {
                list.add(it.getValue());
            }
        });
        return list;
    }

    public void deleteAll() {
        cache.clear();
    }

    public void deleteById(String id) {
        cache.remove(id);
    }

    public MediaStream save(MediaStream ms) {
        cache.put(ms.getId(), ms);
        return ms;
    }

    public MediaStream findById(String id) {
        return cache.get(id);
    }
}
