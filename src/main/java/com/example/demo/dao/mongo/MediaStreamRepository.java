package com.example.demo.dao.mongo;

import com.example.demo.model.MediaStream;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MediaStreamRepository extends CrudRepository<MediaStream, String> {
    List<MediaStream> findByDeploymentId(String deploymentId);
}
