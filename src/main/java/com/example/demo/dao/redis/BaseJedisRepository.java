package com.example.demo.dao.redis;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.io.IOException;

public abstract class BaseJedisRepository {

    public static final String TYPE_PREFIX = "type";
    public static final String SEPARATOR = "/";

    protected final ObjectMapper objectMapper;
    protected final JedisPool jedisPool;
    protected final int dbIndex;

    protected BaseJedisRepository(JedisPool jedisPool, int dbIndex){
        this.jedisPool = jedisPool;
        this.dbIndex = dbIndex;
        this.objectMapper = new ObjectMapper();
        this.objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        this.objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }

    protected Jedis getConnection(){
        Jedis toRet = jedisPool.getResource();
        if(this.dbIndex > 0){
            toRet.select(this.dbIndex);
        }
        return toRet;
    }

    @SneakyThrows(IOException.class)
    protected  <T> T decode(String in, Class<T> targetClass){
        return in == null ? null : objectMapper.readValue(in, targetClass);
    }

    @SneakyThrows(IOException.class)
    protected <T> String encode(T in){
        return in == null ? null : objectMapper.writeValueAsString(in);
    }

    public void deleteAll() {
        try (Jedis jedis = getConnection()) {
            jedis.flushDB();
        }
    }
}
