package com.example.demo.dao.redis;

import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class JedisHelper {

    public static JedisPool pool(String host, int port){
        return new JedisPool(new JedisPoolConfig(), host, port);
    }

}
