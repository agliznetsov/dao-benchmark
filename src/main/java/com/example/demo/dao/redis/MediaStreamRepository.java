package com.example.demo.dao.redis;

import com.example.demo.model.MediaStream;
import com.example.demo.model.OFGroup;
import com.example.demo.model.OFRule;
import com.github.jedis.lock.JedisLock;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface MediaStreamRepository {

    List<MediaStream> getMediaStreams();
    List<MediaStream> getMediaStreams(Set<String> ids);
    List<MediaStream> getMediaStreamsByDeploymentId(String deploymentId);
    MediaStream getMediaStream(String id);
    void deleteMediaStream(String id);
    MediaStream findMediaStream(String id);
    void saveMediaStream(MediaStream ms, Set<String> removedDestinationIds);
    JedisLock lockMediaStream(String id, boolean soft);
    void releaseLockedMediaStream(JedisLock lock);
    void saveMediaStreamRules(String mediaStreamId, Set<OFRule> rules);
    Set<OFRule> getMediaStreamRules(String mediaStreamId);
    void saveMediaStreamGroups(String mediaStreamId, Set<OFGroup> groups);
    Set<OFGroup> getMediaStreamGroups(String mediaStreamId);
    void deleteAll();
    String resolveSourceForDestination(String destId);
    Map<String,String> resolveRedundantMapping(String srcEndpointId, String dstEndpointId);
    void saveRedundantMapping(String primarySrcEndpointId, String primaryDstEndpointId, String secondarySrcEndpointId, String secondaryDstEndpointId);
    void removeRedundantEndpoint(String srcEndpointId, String dstEndpointId);

}
