package com.example.demo.dao.redis;

import com.example.demo.model.MediaStream;
import com.example.demo.model.OFGroup;
import com.example.demo.model.OFRule;
import com.github.jedis.lock.JedisLock;
import lombok.SneakyThrows;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.Transaction;

import java.util.*;
import java.util.stream.Collectors;

public class MediaStreamRepositoryImpl extends BaseJedisRepository implements MediaStreamRepository {

    private static final String MEDIASTREAM_TYPE = "MediaStream";
    private static final String MEDIASTREAM_DESTINATION_TYPE = "MediaStreamDestination";
    private static final String MEDIASTREAM_REDUNDANT_MAPPING_TYPE = "MediaStreamRedundantMapping";
    private static final String KEY_MEDIASTREAM_IDS = TYPE_PREFIX + SEPARATOR + MEDIASTREAM_TYPE;
    private static final String KEY_DEPLOYMENT_ID = "Deployment" + SEPARATOR + "%s";
    private static final String KEY_MEDIASTREAM = MEDIASTREAM_TYPE + SEPARATOR + "%s";
    private static final String KEY_MEDIASTREAM_LOCK = MEDIASTREAM_TYPE + SEPARATOR + "%s" + SEPARATOR + "lock";
    private static final String KEY_MEDIASTREAM_RULES = KEY_MEDIASTREAM + SEPARATOR + "rules";
    private static final String KEY_MEDIASTREAM_GROUPS = KEY_MEDIASTREAM + SEPARATOR + "groups";
    private static final String KEY_DESTINATION = MEDIASTREAM_DESTINATION_TYPE + SEPARATOR + "%s";
    private static final String KEY_MEDIASTREAM_REDUNDANT_MAPPING = MEDIASTREAM_REDUNDANT_MAPPING_TYPE + SEPARATOR + "%s";

    public MediaStreamRepositoryImpl(JedisPool jedisPool, int dbIndex){
        super(jedisPool, dbIndex);
    }

    @Override
    public List<MediaStream> getMediaStreams() {
        try (Jedis jedis = getConnection()) {
            Set<String> keys = jedis.smembers(KEY_MEDIASTREAM_IDS);
            if (keys.size() > 0) {
                return jedis.mget(keys.toArray(new String[0]))
                        .stream()
                        .map(s -> decode(s, MediaStream.class))
                        .filter(Objects::nonNull)
                        .collect(Collectors.toList());
            } else {
                return Collections.emptyList();
            }
        }
    }

    @Override
    public List<MediaStream> getMediaStreamsByDeploymentId(String deploymentId) {
        try (Jedis jedis = getConnection()) {
            String key = String.format(KEY_DEPLOYMENT_ID, deploymentId);
            Set<String> keys = jedis.smembers(key);
            if (keys.size() > 0) {
                return jedis.mget(keys.toArray(new String[0]))
                        .stream()
                        .map(s -> decode(s, MediaStream.class))
                        .filter(Objects::nonNull)
                        .collect(Collectors.toList());
            } else {
                return Collections.emptyList();
            }
        }
    }

    @Override
    public List<MediaStream> getMediaStreams(Set<String> ids){
        try (Jedis jedis = getConnection()) {
            List<String> keys = ids.stream().map(id -> String.format(KEY_MEDIASTREAM, id)).collect(Collectors.toList());
            List<String> values = jedis.mget(keys.toArray(new String[]{}));
            return values
                    .stream()
                    .map(value -> decode(value, MediaStream.class))
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
        }
    }

    @Override
    public MediaStream getMediaStream(String id){
        try (Jedis jedis = getConnection()) {
            String key = String.format(KEY_MEDIASTREAM, id);
            if (!jedis.exists(key)) {
                throw new RuntimeException(String.format("Media stream not found with id %s", id));
            }
            return decode(jedis.get(key), MediaStream.class);
        }
    }

    @Override
    public void deleteMediaStream(String id){
        try (Jedis jedis = getConnection()) {
            String key = String.format(KEY_MEDIASTREAM, id);
            if (jedis.exists(key)) {
                MediaStream ms = decode(jedis.get(key), MediaStream.class);
                Transaction t = jedis.multi();
                t.del(key);
                t.del(String.format(KEY_MEDIASTREAM_RULES, id));
                t.del(String.format(KEY_MEDIASTREAM_GROUPS, id));
                t.srem(KEY_MEDIASTREAM_IDS, key);
                if(ms.getDeploymentId() != null) {
                    String deploymentKey = String.format(KEY_DEPLOYMENT_ID, ms.getDeploymentId());
                    t.srem(deploymentKey, key);
                }
                ms.getDestinationEndpoints().forEach(dst -> {
                    t.del(String.format(KEY_DESTINATION, dst));
                });
                t.exec();
            }
        }
    }

    @Override
    public MediaStream findMediaStream(String id){
        try (Jedis jedis = getConnection()) {
            String key = String.format(KEY_MEDIASTREAM, id);
            String value = jedis.get(key);
            if (value != null) {
                return decode(value, MediaStream.class);
            } else {
                return null;
            }
        }
    }

    @Override
    public void saveMediaStream(MediaStream ms, Set<String> removedDestinationIds) {
        try (Jedis jedis = getConnection()) {
            String key = String.format(KEY_MEDIASTREAM, ms.getId());
            Transaction t = jedis.multi();
            t.set(key, encode(ms));
            t.sadd(KEY_MEDIASTREAM_IDS, key);
            if(ms.getDeploymentId() != null) {
                String deploymentKey = String.format(KEY_DEPLOYMENT_ID, ms.getDeploymentId());
                t.sadd(deploymentKey, key);
            }
            ms.getDestinationEndpoints().forEach(dst -> {
                t.set(String.format(KEY_DESTINATION, dst), ms.getId());
            });
            removedDestinationIds.forEach(dst -> {
                t.del(String.format(KEY_DESTINATION, dst));
            });
            t.exec();
        }
    }

    @Override
    @SneakyThrows(InterruptedException.class)
    public JedisLock lockMediaStream(String id, boolean soft){
        try (Jedis jedis = getConnection()) {
            String key = String.format(KEY_MEDIASTREAM, id);
            if (jedis.exists(key)) {
                JedisLock lock = new JedisLock(String.format(KEY_MEDIASTREAM_LOCK, id), 5000, 20000);
                if (lock.acquire(jedis)) {
                    return lock;
                } else {
                    throw new RuntimeException(String.format("Could not obtain lock for media stream %s", id));
                }
            } else {
                if (soft) {
                    return null;
                } else {
                    throw new RuntimeException(String.format("Media stream not found with id %s", id));
                }
            }
        }
    }

    @Override
    public void releaseLockedMediaStream(JedisLock lock) {
        try (Jedis jedis = getConnection()) {
            lock.release(jedis);
        }
    }

    @Override
    public void saveMediaStreamRules(String mediaStreamId, Set<OFRule> rules){
        try (Jedis jedis = getConnection()) {
            String key = String.format(KEY_MEDIASTREAM_RULES, mediaStreamId);
            if (rules.size() > 0) {
                Transaction t = jedis.multi();
                t.del(key);
                t.sadd(key, rules.stream().map(this::encode).toArray(String[]::new));
                t.exec();
            } else {
                jedis.del(key);
            }
        }
    }

    @Override
    public Set<OFRule> getMediaStreamRules(String mediaStreamId){
        try (Jedis jedis = getConnection()) {
            String key = String.format(KEY_MEDIASTREAM_RULES, mediaStreamId);
            return jedis.smembers(key).stream()
                .map(s -> decode(s, OFRule.class))
                .collect(Collectors.toSet());
        }
    }

    @Override
    public void saveMediaStreamGroups(String mediaStreamId, Set<OFGroup> groups) {
        try (Jedis jedis = getConnection()) {
            String key = String.format(KEY_MEDIASTREAM_GROUPS, mediaStreamId);
            if (groups.size() > 0) {
                Transaction t = jedis.multi();
                t.del(key);
                t.sadd(key, groups.stream().map(this::encode).toArray(String[]::new));
                t.exec();
            } else {
                jedis.del(key);
            }
        }
    }

    @Override
    public Set<OFGroup> getMediaStreamGroups(String mediaStreamId) {
        try (Jedis jedis = getConnection()) {
            String key = String.format(KEY_MEDIASTREAM_GROUPS, mediaStreamId);
            return jedis.smembers(key).stream()
                .map(s -> decode(s, OFGroup.class))
                .collect(Collectors.toSet());
        }
    }

    @Override
    public String resolveSourceForDestination(String destId){
        try (Jedis jedis = getConnection()) {
            String key = String.format(KEY_DESTINATION, destId);
            return jedis.get(key);
        }
    }

    @Override
    public Map<String,String> resolveRedundantMapping(String srcEndpointId, String dstEndpointId) {
        try (Jedis jedis = getConnection()) {
            String key = String.format(KEY_MEDIASTREAM_REDUNDANT_MAPPING, getPathId(srcEndpointId, dstEndpointId));
            return splitPathIds(jedis.smembers(key));
        }
    }

    @Override
    public void saveRedundantMapping(String primarySrcEndpointId, String primaryDstEndpointId, String secondarySrcEndpointId, String secondaryDstEndpointId) {
        try (Jedis jedis = getConnection()) {
            String primaryPathId = getPathId(primarySrcEndpointId, primaryDstEndpointId);
            String secondaryPathId = getPathId(secondarySrcEndpointId, secondaryDstEndpointId);
            Set<String> endpointIds = new HashSet<>();
            endpointIds.add(primaryPathId);
            endpointIds.add(secondaryPathId);
            String primaryKey = String.format(KEY_MEDIASTREAM_REDUNDANT_MAPPING, primaryPathId);
            endpointIds.addAll(jedis.smembers(primaryKey));
            Transaction t = jedis.multi();
            endpointIds.forEach(endpointId -> {
                String key = String.format(KEY_MEDIASTREAM_REDUNDANT_MAPPING, endpointId);
                t.del(key);
                endpointIds
                        .stream()
                        .filter(x -> !x.equals(endpointId))
                        .forEach(x -> t.sadd(key, x));
            });
            t.exec();
        }
    }

    @Override
    public void removeRedundantEndpoint(String srcEndpointId, String dstEndpointId) {
        try (Jedis jedis = getConnection()) {
            String primaryPathId = getPathId(srcEndpointId, dstEndpointId);
            String primaryKey = String.format(KEY_MEDIASTREAM_REDUNDANT_MAPPING, primaryPathId);
            Set<String> secondaryEndpointIds = jedis.smembers(primaryKey);
            Transaction t = jedis.multi();
            t.del(primaryKey);
            if(secondaryEndpointIds.size() > 1) {
                secondaryEndpointIds.forEach(secondaryEndpointId -> {
                    String key = String.format(KEY_MEDIASTREAM_REDUNDANT_MAPPING, secondaryEndpointId);
                    t.srem(key, primaryPathId);
                });
            } else if(secondaryEndpointIds.size() == 1){
                String key = String.format(KEY_MEDIASTREAM_REDUNDANT_MAPPING, secondaryEndpointIds.iterator().next());
                t.del(key);
            }
            t.exec();
        }
    }


    private static final String PATH_DELIMETER = ";";

    private String getPathId(String srcId, String dstId) {
        return srcId + PATH_DELIMETER + dstId;
    }

    private Map<String,String> splitPathIds(Set<String> keys) {
        return keys
                .stream()
                .collect(Collectors.toMap(x -> x.split(PATH_DELIMETER)[0], x -> x.split(PATH_DELIMETER)[1]));
    }
}
