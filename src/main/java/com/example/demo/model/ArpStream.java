package com.example.demo.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@NoArgsConstructor
@EqualsAndHashCode(of = {"id"})
public class ArpStream {

    private String id;

    private int priority = 10000;

    private Set<OFPort> sources = new HashSet<>();

    private Set<OFPort> destinations = new HashSet<>();

    private List<List<DirectedSwitchLink>> paths = new ArrayList<>();

}
