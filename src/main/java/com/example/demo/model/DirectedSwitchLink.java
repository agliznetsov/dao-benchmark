package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.List;

@Data
@EqualsAndHashCode(of = {"src", "dst"})
@NoArgsConstructor
public class DirectedSwitchLink {

    private OFPort src;
    private OFPort dst;
    private long maxBandwidthInBps = 0;
    private boolean local = false;

    public DirectedSwitchLink(OFPort src, OFPort dst){
        this.src = src;
        this.dst = dst;
    }

    public DirectedSwitchLink(OFPort src, OFPort dst, long maxBandwidthInBps){
        this(src,dst);
        this.maxBandwidthInBps = maxBandwidthInBps;
    }

    public DirectedSwitchLink(OFPort src, OFPort dst, boolean local){
        this(src,dst);
        this.local = local;
    }

    @JsonIgnore
    public String getLinkId(){
        List<String> ports = Arrays.asList(src.getId(), dst.getId());
        ports.sort(String::compareTo);
        return String.join(",", ports);
    }

}
