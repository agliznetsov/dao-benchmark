package com.example.demo.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class FlowStats {

    private long nrOfBytes = 0;
    private long nrOfPackets = 0;

}
