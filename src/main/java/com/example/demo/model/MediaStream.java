package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Document
@NoArgsConstructor
public class MediaStream implements Serializable {

    @Id
    private String documentId;

    // metadata
    private String sourceName;

    @Indexed
    private String deploymentId;

    private String externalSourceId;

    private MediaStreamEndpoint source;
    private long bandwidthInBps;

    // key = destNetworkLocationId, value = paths to dest
    private Map<String,List<MediaStreamPath>> paths = new HashMap<>();

    public MediaStream(MediaStreamEndpoint source, long bandwidthInBps){
        this.documentId = source.getId();
        this.source = source;
        this.bandwidthInBps = bandwidthInBps;
    }

    public long getBandwidthInBps(){
        return this.bandwidthInBps;
    }

    public void setBandwidthInBps(long bandwidthInBps) {
        this.bandwidthInBps = bandwidthInBps;
    }

    public MediaStreamEndpoint getSource(){
        return this.source;
    }

    public Map<String,List<MediaStreamPath>> getPaths(){
        return this.paths;
    }

    public String getSourceName() {
        return this.sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public String getDeploymentId() {
        return deploymentId;
    }

    public void setDeploymentId(String deploymentId) {
        this.deploymentId = deploymentId;
    }

    public String getExternalSourceId() {
        return externalSourceId;
    }

    public void setExternalSourceId(String externalSourceId) {
        this.externalSourceId = externalSourceId;
    }

    @JsonIgnore
    public String getId() {
        return source.getId();
    }

    @JsonIgnore
    public List<MediaStreamPath> getPathList() {
        return paths.values()
                .stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    @JsonIgnore
    public void addPath(String name, List<DirectedSwitchLink> path, StreamId dstStreamId, boolean staticPath, String externalDestinationId){
        if(path.size() == 0){
            return;
        }
        OFPort src = path.get(0).getSrc();
        OFPort dst = path.get(path.size()-1).getDst();
        if(!this.source.getNetworkLocation().equals(src)){
            throw new IllegalArgumentException("Path needs to start in source " + this.source.getNetworkLocation());
        }
        if(!this.paths.containsKey(dst.getId())){
            this.paths.put(dst.getId(), new ArrayList<>());
        }
        this.paths.get(dst.getId()).add(new MediaStreamPath(name, path, dstStreamId, staticPath, externalDestinationId));
    }

    @JsonIgnore
    public Set<DirectedSwitchLink> removePath(String pathId, StreamId dstStreamId){
        if(!paths.containsKey(pathId)){
            throw new IllegalArgumentException("Path to destination " + pathId + " not found");
        }

        MediaStreamPath path = paths.get(pathId)
                .stream()
                .filter(p -> p.getDstStreamId().equals(dstStreamId))
                .findFirst()
                .orElse(null);
        if(path == null) {
            throw new IllegalArgumentException("Path to destination " + pathId + " and streamId " + dstStreamId.toString() + " not found");
        }

        paths.get(pathId).remove(path);
        if(paths.get(pathId).size() == 0) {
            paths.remove(pathId);
        }
        Set<DirectedSwitchLink> remainingLocalLinks = getLinks(true);
        Set<DirectedSwitchLink> affectedCandidates = path.getLinks()
                .stream()
                .filter(dsl -> !dsl.isLocal())
                .collect(Collectors.toSet());
        affectedCandidates.removeIf(remainingLocalLinks::contains);
        return affectedCandidates;
    }

    @JsonIgnore
    public void removeAllPaths(){
        paths.clear();
    }

    @JsonIgnore
    public Set<String> getNetworkDestinations(){
        return this.paths.keySet();
    }

    @JsonIgnore
    public Map<String,List<DirectedSwitchLink>> getPathByDestination(){
        Map<String,List<DirectedSwitchLink>> toRet = new HashMap<>();
        this.paths.forEach((k,v) -> toRet.put(k, v.get(0).getLinks()));
        return toRet;
    }

    @JsonIgnore
    public MediaStreamPath findPathByDestination(MediaStreamEndpoint destination) {
        String destinationNetworkId = destination.getNetworkLocation().getId();
        return !this.paths.containsKey(destinationNetworkId) ? null : this.paths.get(destinationNetworkId)
                .stream()
                .filter(msp -> destination.getStreamId().equals(msp.getDstStreamId()))
                .findFirst()
                .orElse(null);
    }

    @JsonIgnore
    public Set<String> getDestinationEndpoints() {
        Set<String> toRet = new HashSet<>();
        this.paths.forEach((dstNetworkId, paths) -> {
            paths.forEach(path -> {
                MediaStreamEndpoint endpoint = new MediaStreamEndpoint(path.getDstStreamId(), OFPort.fromString(dstNetworkId));
                toRet.add(endpoint.getId());
            });
        });
        return toRet;
    }

    @JsonIgnore
    public Set<DirectedSwitchLink> getLinks(){
        return this.paths.values()
                .stream()
                .flatMap(Collection::stream)
                .flatMap(x -> x.getLinks().stream())
                .collect(Collectors.toSet());
    }

    @JsonIgnore
    public Set<DirectedSwitchLink> getLinks(boolean local){
        return this.paths.values()
                .stream()
                .flatMap(Collection::stream)
                .flatMap(x -> x.getLinks().stream())
                .filter(x -> x.isLocal() == local)
                .collect(Collectors.toSet());
    }

    @JsonIgnore
    public Set<String> getSwitchIds() {
        return this.paths.values()
                .stream()
                .flatMap(Collection::stream)
                .flatMap(x -> x.getLinks().stream())
                .flatMap(p -> Stream.of(p.getSrc().getSwitchId(), p.getDst().getSwitchId()))
                .collect(Collectors.toSet());
    }

}
