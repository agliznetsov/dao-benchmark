package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class MediaStreamEndpoint {

    private StreamId streamId;
    private OFPort networkLocation;

    @JsonIgnore
    public String getId(){
        return streamId.getId() + "_" + networkLocation.getId();
    }

    public static MediaStreamEndpoint fromString(String str) {
        String[] parts = str.split("_");
        return new MediaStreamEndpoint(StreamId.fromString(parts[0]), OFPort.fromString(parts[1]));
    }
}
