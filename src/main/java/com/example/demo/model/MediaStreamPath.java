package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = {"links", "dstStreamId"})
public class MediaStreamPath {

    // metadata
    private String name;

    private List<DirectedSwitchLink> links = new ArrayList<>();
    private StreamId dstStreamId;
    private boolean staticPath = false;

    // metadata
    private String externalDestinationId;

    @JsonIgnore
    public OFPort getDstNetworkId(){
        return links.get(links.size()-1).getDst();
    }

    @JsonIgnore
    public OFPort getSrcNetworkId(){
        return links.get(0).getSrc();
    }

}
