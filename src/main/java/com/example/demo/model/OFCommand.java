package com.example.demo.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
public class OFCommand {

    private Set<OFRule> rulesToAdd = new HashSet<>();
    private Set<OFRule> rulesToUpdate = new HashSet<>();
    private Set<OFRule> rulesToDelete = new HashSet<>();

    private Set<OFGroup> groupsToAdd = new HashSet<>();
    private Set<OFGroup> groupsToUpdate = new HashSet<>();
    private Set<OFGroup> groupsToDelete = new HashSet<>();

    public OFCommand(Set<OFRule> rulesToAdd, Set<OFRule> rulesToDelete) {
        this.rulesToAdd = rulesToAdd;
        this.rulesToDelete = rulesToDelete;
    }

    public void merge(OFCommand command) {
        this.rulesToAdd.addAll(command.getRulesToAdd());
        this.rulesToUpdate.addAll(command.getRulesToUpdate());
        this.rulesToDelete.addAll(command.getRulesToDelete());
        this.groupsToAdd.addAll(command.getGroupsToAdd());
        this.groupsToUpdate.addAll(command.getGroupsToUpdate());
        this.groupsToDelete.addAll(command.getGroupsToDelete());
    }

    public boolean isEmpty() {
        return rulesToAdd.size() == 0 &&
            rulesToUpdate.size() == 0 &&
            rulesToDelete.size() == 0 &&
            groupsToAdd.size() == 0 &&
            groupsToUpdate.size() == 0 &&
            groupsToDelete.size() == 0;
    }
}
