package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Data
@EqualsAndHashCode(of = {"switchId", "buckets"})
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OFGroup {

    private String switchId;
    private int groupId;
    private List<OFBucket> buckets = new ArrayList<>();

    // stats
    private Long durationInMs;
    private Long packetCount;
    private Long byteCount;
}

