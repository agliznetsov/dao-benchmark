package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@EqualsAndHashCode(of = {"switchId", "port"})
@ToString(of = {"switchId", "port"})
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OFPort {

    private String switchId;
    private int port;
    private String portName;

    //stats
    private Long durationInMs;
    private Long nrOfRxPackets;
    private Long nrOfTxPackets;
    private Long nrOfRxBytes;
    private Long nrOfTxBytes;
    private Long nrOfRxDropped;
    private Long nrOfTxDropped;
    private Long nrOfRxErrors;
    private Long nrOfTxErrors;

    public OFPort(String switchId, int port) {
        this.switchId = switchId;
        setPort(port);
    }

    public void setPort(int port) {
        this.port = port;
        this.portName = toPortName(port);
    }

    @JsonIgnore
    public String getId(){
        return switchId + "-" + port;
    }

    public static OFPort fromString(String str) {
        String[] parts = str.split("-");
        return new OFPort(parts[0], Integer.parseInt(parts[1]));
    }

    private String toPortName(int portNr) {
        switch(portNr) {
            case 256:
                return "max";
            case -8:
                return "in_port";
            case -7:
                return "table";
            case -6:
                return "normal";
            case -5:
                return "flood";
            case -4:
                return "all";
            case -3:
                return "controller";
            case -2:
                return "local";
            case -1:
                return "any";
            default:
                return null;
        }
    }
}
