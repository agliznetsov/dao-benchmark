package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Data
@EqualsAndHashCode(of = {"switchId", "tableId", "priority", "cookie", "match", "actions"})
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OFRule {

    private String switchId;
    private int tableId = 0;
    private int priority = 32768;
    private long cookie = 0;
    private OFRuleMatch match = new OFRuleMatch();
    private List<OFRuleAction> actions = new ArrayList<>();
    private List<OFRuleInstruction> instructions = new ArrayList<>();

    // stats
    private Long durationInMs;
    private Long packetCount;
    private Long byteCount;

}

