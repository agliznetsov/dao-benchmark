package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@EqualsAndHashCode
@ToString
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OFRuleAction {

    public static final int IN_PORT = -8;
    public static final int CONTROLLER_PORT = -3;

    private ActionType type;

    // type = OUTPUT
    private Integer port;

    // type = GROUP
    private Integer groupId;

    // type = SET_FIELD
    private SetField field;
    private Object fieldvalue;

    public static OFRuleAction toInPort() {
        OFRuleAction toRet = new OFRuleAction();
        toRet.setType(ActionType.OUTPUT);
        toRet.setPort(IN_PORT);
        return toRet;
    }

    public static OFRuleAction toController() {
        OFRuleAction toRet = new OFRuleAction();
        toRet.setType(ActionType.OUTPUT);
        toRet.setPort(CONTROLLER_PORT);
        return toRet;
    }

    public static OFRuleAction toPort(int outPort) {
        OFRuleAction toRet = new OFRuleAction();
        toRet.setType(ActionType.OUTPUT);
        toRet.setPort(outPort);
        return toRet;
    }

    public static OFRuleAction toGroup(int groupId) {
        OFRuleAction toRet = new OFRuleAction();
        toRet.setType(ActionType.GROUP);
        toRet.setGroupId(groupId);
        return toRet;
    }

    public static OFRuleAction setField(SetField field, Object value) {
        if(!field.valueClass.equals(value.getClass())) {
            throw new IllegalArgumentException("Incompatible value type. Expected " + field.valueClass.getSimpleName() + " but got " + value.getClass().getSimpleName());
        }
        OFRuleAction toRet = new OFRuleAction();
        toRet.setType(ActionType.SET_FIELD);
        toRet.setField(field);
        toRet.setFieldvalue(value);
        return toRet;
    }

    public enum ActionType {
        OUTPUT,
        GROUP,
        SET_FIELD
    }

    public enum SetField {
        UDP_DST(Integer.class),
        ETH_DST(String.class),
        IPV4_DST(String.class),
        IPV4_SRC(String.class);

        Class valueClass;

        SetField(Class valueClass) {
            this.valueClass = valueClass;
        }
    }
}

