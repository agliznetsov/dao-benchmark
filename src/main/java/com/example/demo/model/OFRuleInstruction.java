package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@EqualsAndHashCode
@ToString
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OFRuleInstruction {

    private InstructionType type;

    // type = table
    private Integer tableId;

    public static OFRuleInstruction toTable(int tableId) {
        OFRuleInstruction toRet = new OFRuleInstruction();
        toRet.setType(InstructionType.TABLE);
        toRet.setTableId(tableId);
        return toRet;
    }

    public enum InstructionType {
        TABLE,
    }

}

