package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OFRuleMatch {

    private Integer inPort;

    private String ethSrc;
    private String ethDst;
    private EthType ethType;

    private IpProto ipProto; // prereq: ethType
    private String ip4Src;   // prereq: ethType=IPV4
    private String ip4Dst;   // prereq: ethType=IPV4
    private String ip6Src;   // prereq: ethType=IPV6
    private String ip6Dst;   // prereq: ethType=IPV6

    private Integer tcpSrc;  // prereq: ipProto=TCP
    private Integer tcpDst;  // prereq: ipProto=TCP
    private Integer udpSrc;  // prereq: ipProto=UDP
    private Integer udpDst;  // prereq: ipProto=UDP


    public enum EthType {
        IPV4,
        IPV6,
        LLDP,
        ARP
    }

    public enum IpProto {
        UDP,
        TCP
    }

}

