package com.example.demo.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@EqualsAndHashCode(of = {"id"})
public class PTPStream {

    private String id;

    private Integer priority;

    private String ipAddress = "224.0.1.129";

    private String switchId;

    private List<OFPort> sources = new ArrayList<>();
    private List<OFPort> destinations = new ArrayList<>();

    private List<OFPort> endpoints = new ArrayList<>();

    private List<List<DirectedSwitchLink>> paths = new ArrayList<>();

    public PTPStream(String switchId, List<OFPort> endpoints){
        this.switchId = switchId;
        this.endpoints = endpoints;
    }

    public PTPStream(String switchId, List<OFPort> sources, List<OFPort> destinations){
        this.switchId = switchId;
        this.sources = sources;
        this.destinations = destinations;
    }
}
