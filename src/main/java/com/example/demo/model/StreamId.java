package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(of = {"ipAddress", "ipPort"})
public class StreamId {

    private String ipAddress;
    private int ipPort = 0;
    private String srcIpAddress; // only used for NAT-ing of devices that require the source IP address

    public StreamId(String ipAddress, int ipPort) {
        this.ipAddress = ipAddress;
        this.ipPort = ipPort;
    }

    @JsonIgnore
    public String getId(){
        return ipAddress + ":" + ipPort;
    }

    public static StreamId fromString(String str) {
        String[] parts = str.split(":");
        return new StreamId(parts[0], Integer.parseInt(parts[1]));
    }

}
