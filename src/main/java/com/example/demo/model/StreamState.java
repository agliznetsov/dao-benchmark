package com.example.demo.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class StreamState {

    private List<MediaStream> mediaStreams = new ArrayList<>();
    private List<PTPStream> ptpStreams = new ArrayList<>();

}
