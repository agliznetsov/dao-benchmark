package com.example.demo.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;

@Data
@EqualsAndHashCode(of = {"id"})
public class Switch {

    private String id;

    private String manufacturer;

    private String hardwareVersion;

    private String softwareVersion;

    private String serialNumber;

    private String datapathDescription;

    private List<SwitchPort> ports = new ArrayList<>();

    public Switch(String id){
        this.id = id;
    }

}
