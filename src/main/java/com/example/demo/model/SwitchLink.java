package com.example.demo.model;

import lombok.Data;
import lombok.ToString;

@Data
@ToString(of = {"p1", "p2"})
public class SwitchLink {

    protected OFPort p1;
    protected OFPort p2;
    protected long maxBandwidthInBps = 0;

    public long getMaxBandwidthInGbps() {
        return maxBandwidthInBps / 1000000000;
    }

    public SwitchLink(OFPort p1, OFPort p2){
        this.p1 = p1;
        this.p2 = p2;
    }

    public SwitchLink(OFPort p1, OFPort p2, long maxBandwidthInBps){
        this.p1 = p1;
        this.p2 = p2;
        this.maxBandwidthInBps = maxBandwidthInBps;
    }

    public OFPort connectedOFPort(String switchId){
        if(p1.getSwitchId().equals(switchId)){
            return p2;
        } else if(p2.getSwitchId().equals(switchId)){
            return p1;
        } else {
            throw new IllegalArgumentException("Invalid switchId " + switchId + " for this link.");
        }
    }

    public String connectedSwitchId(String switchId){
        return connectedOFPort(switchId).getSwitchId();
    }

    public OFPort getOFPort(String switchId){
        if(p1.getSwitchId().equals(switchId)){
            return p1;
        } else if(p2.getSwitchId().equals(switchId)){
            return p2;
        } else {
            return null;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SwitchLink link = (SwitchLink) o;

        if(p1.equals(link.p1)){
            return p2.equals(link.p2);
        }
        if(p1.equals(link.p2)){
            return p2.equals(link.p1);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return p1.hashCode() + p2.hashCode();
    }

}
