package com.example.demo.model;

import lombok.Data;

@Data
public class SwitchPort {

    private String name;

    private Integer nr;

    private boolean enabled = false;

    // static variables
    private String state;

    private Long maxBandwidthInBps;

    public Long getMaxBandwidthInGbps() {
        return maxBandwidthInBps != null ? (long) (1.0 * maxBandwidthInBps / 1000000000) : null;
    }

    // dynamic variables

    private String lastBandwidthUpdate;

    private Long currentTxBandwidthInBps;

    private Long currentRxBandwidthInBps;

    public Long getCurrentBandwidthInBps() {
        return currentTxBandwidthInBps != null && currentRxBandwidthInBps != null ? currentTxBandwidthInBps + currentRxBandwidthInBps : null;
    }

    public Long getCurrentBandwidthInGbps() {
        Long currentBandwidthInBps = getCurrentBandwidthInBps();
        return currentBandwidthInBps != null ? (long) (1.0 * currentBandwidthInBps / 1000000000) : null;
    }

    public Long getCurrentBandwidthInMbps() {
        Long currentBandwidthInBps = getCurrentBandwidthInBps();
        return currentBandwidthInBps != null ? (long) (1.0 * currentBandwidthInBps / 1000000) : null;
    }

    public Long getCurrentBandwidthInKbps() {
        Long currentBandwidthInBps = getCurrentBandwidthInBps();
        return currentBandwidthInBps != null ? (long) (1.0 * currentBandwidthInBps / 1000) : null;
    }

}
