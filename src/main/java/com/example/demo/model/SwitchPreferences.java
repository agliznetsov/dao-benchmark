package com.example.demo.model;

import lombok.Data;

@Data
public class SwitchPreferences {

    private String serialNumber;
    private String manufacturer;
    private String hardwareVersion;
    private String softwareVersion;
    private Boolean useOFGroups;
    private boolean natRestricted = false;
    private int localDuplicationLimit = 0;

}
