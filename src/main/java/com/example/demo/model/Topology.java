package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.*;
import java.util.stream.Collectors;

public class Topology {

    private Map<String,Switch> switches = new HashMap<>();
    private Map<String,Set<SwitchLink>> linksPerSwitch = new HashMap<>();

    public List<Switch> getSwitches(){
        List<Switch> toRet = new ArrayList<>(switches.values());
        toRet.sort(Comparator.comparing(Switch::getId));
        return toRet;
    }

    public Set<String> getSwitchIds() {
        return switches.keySet();
    }

    @JsonIgnore
    public Map<String,Set<SwitchLink>> getLinksPerSwitch(){
        return linksPerSwitch;
    }

    public Set<SwitchLink> getLinks() {
        return linksPerSwitch.values()
                .stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());
    }

    public Switch getSwitch(String switchId){
        return switches.get(switchId);
    }

    public Set<SwitchLink> getLinks(String switchId){
        return linksPerSwitch.get(switchId);
    }

    public void addSwitch(Switch s){
        if(!switches.containsKey(s.getId())) {
            switches.put(s.getId(), s);
            linksPerSwitch.put(s.getId(), new HashSet<>());
        }
    }

    public void removeSwitch(String id){
        if(!switches.containsKey(id)){
            throw new IllegalArgumentException("Switch with id " + id + " not found");
        }
        linksPerSwitch.get(id).forEach(link -> linksPerSwitch.get(link.connectedSwitchId(id)).remove(link));
        linksPerSwitch.remove(id);
        switches.remove(id);
    }

    public void addLink(SwitchLink link){
        if(!switches.containsKey(link.getP1().getSwitchId())){
            throw new IllegalArgumentException("Switch with id " + link.getP1().getSwitchId() + " not found");
        }
        if(!switches.containsKey(link.getP2().getSwitchId())){
            throw new IllegalArgumentException("Switch with id " + link.getP2().getSwitchId() + " not found");
        }
        linksPerSwitch.get(link.getP1().getSwitchId()).add(link);
        linksPerSwitch.get(link.getP2().getSwitchId()).add(link);
    }

    public void removeLink(SwitchLink link){
        if(!switches.containsKey(link.getP1().getSwitchId())){
            throw new IllegalArgumentException("Switch with id " + link.getP1().getSwitchId() + " not found");
        }
        if(!switches.containsKey(link.getP2().getSwitchId())){
            throw new IllegalArgumentException("Switch with id " + link.getP2().getSwitchId() + " not found");
        }
        linksPerSwitch.get(link.getP1().getSwitchId()).remove(link);
        linksPerSwitch.get(link.getP2().getSwitchId()).remove(link);
    }

}
