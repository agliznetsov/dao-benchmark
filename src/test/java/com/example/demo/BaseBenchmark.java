package com.example.demo;

import com.example.demo.model.MediaStream;
import com.example.demo.model.MediaStreamEndpoint;
import com.example.demo.model.OFPort;
import com.example.demo.model.StreamId;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

@Slf4j
public abstract class BaseBenchmark {
    protected static final String DEPLOYMENT1 = "deployment1";
    protected static final String DEPLOYMENT2 = "deployment2";

    protected List<String> streams = new ArrayList<>();
    protected Random random = new Random();

    private ExecutorService executor = Executors.newCachedThreadPool();


    protected void measure(String name, Runnable runnable) {
        runnable.run();
        for (int i = 0; i < 3; i++) {
            long start = System.currentTimeMillis();
            long count = 0;
            while (System.currentTimeMillis() - start < 5000) {
                runnable.run();
                count++;
            }
            long end = System.currentTimeMillis();
            double op = (end - start) * 1.0 / count;
            System.out.println(String.format("%s iteration #%d %.6f ms", name, i, op));
        }
    }

    protected void saveStreams(int count, String deploymentId) {
        streams.clear();
        for (int i = 0; i < count; i++) {
            streams.add(saveStream(deploymentId).getId());
        }
    }

    protected MediaStream saveStream(String deploymentId) {
        MediaStream ms = new MediaStream(new MediaStreamEndpoint(new StreamId("ip", 8000), new OFPort("1", 1)), 1000);
        ms.setDeploymentId(deploymentId);
        return saveMediaStream(ms);
    }

    protected MediaStream findRandomStream() {
        String id = streams.get(random.nextInt(streams.size()));
        return loadMediaStream(id);
    }

    protected void findMaxThroughput() {
        saveStreams(1000, DEPLOYMENT1);
        int threadCount = 1;
        while (threadCount <= 16) {
            long throughput = testThroughput(threadCount);
            System.out.println(String.format("Threads %d throughput %d/sec", threadCount, throughput));
            threadCount *= 2;
        }
    }

    protected long testThroughput(int threadCount) {
        List<Future<ClientRun>> clients = new ArrayList<>();
        for (int i = 0; i < threadCount; i++) {
            clients.add(executor.submit(new Client()));
        }
        List<ClientRun> results = clients.stream().map(this::resolveResult).collect(Collectors.toList());
        long totalCount = results.stream().mapToLong(it -> it.count).sum();
        long totalTime = results.stream().mapToLong(it -> it.time).sum();
        return totalCount * 1000 / totalTime;
    }

    @SneakyThrows
    private ClientRun resolveResult(Future<ClientRun> future) {
        return future.get();
    }

    protected abstract MediaStream saveMediaStream(MediaStream ms);

    protected abstract MediaStream loadMediaStream(String id);

    protected class Client implements Callable<ClientRun> {
        @Override
        public ClientRun call() throws Exception {
            long start = System.currentTimeMillis();
            long count = 0;
            while (System.currentTimeMillis() - start < 5000) {
                MediaStream ms = findRandomStream();
                ms.setBandwidthInBps(ms.getBandwidthInBps() + 1);
                saveMediaStream(ms);
                count++;
            }
            long end = System.currentTimeMillis();
            return new ClientRun(end - start, count);
        }
    }

    @AllArgsConstructor
    protected static class ClientRun {
        final long time;
        final long count;
    }
}
