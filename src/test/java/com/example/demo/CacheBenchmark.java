package com.example.demo;

import com.example.demo.dao.cache.MediaStreamRepository;
import com.example.demo.model.MediaStream;
import org.junit.Before;
import org.junit.Test;


public class CacheBenchmark extends BaseBenchmark {

    MediaStreamRepository mediaStreamRepository = new MediaStreamRepository();

    @Before
    public void init() {
        streams.clear();
        mediaStreamRepository.deleteAll();
    }

    @Test
    public void testSaveMediaStream() {
        measure("saveMediaStream", () -> saveStream(DEPLOYMENT1));
    }

    @Test
    public void testFindMediaStream() {
        saveStreams(1000, DEPLOYMENT1);
        measure("findMediaStream", this::findRandomStream);
    }

    @Test
    public void testFindMediaStreamsByDeploymentId() {
        saveStreams(1000, DEPLOYMENT1);
        saveStreams(10, DEPLOYMENT2);
        measure("findByDeploymentId", () -> mediaStreamRepository.findByDeploymentId(DEPLOYMENT2));
    }

    @Test
    public void testDeleteMediaStream() {
        for (int i = 0; i < 3; i++) {
            streams.clear();
            saveStreams(10000, DEPLOYMENT1);
            long start = System.currentTimeMillis();
            for (String id : streams) {
                mediaStreamRepository.deleteById(id);
            }
            long end = System.currentTimeMillis();
            double op = (end - start) * 1.0 / streams.size();
            System.out.println(String.format("%s iteration #%d %.6f ms", "deleteMediaStream", i, op));
        }
    }

    @Test
    public void testThroughput() {
        findMaxThroughput();
    }

    @Override
    protected MediaStream saveMediaStream(MediaStream ms) {
        return mediaStreamRepository.save(ms);
    }

    @Override
    protected MediaStream loadMediaStream(String id) {
        return mediaStreamRepository.findById(id);
    }

}
