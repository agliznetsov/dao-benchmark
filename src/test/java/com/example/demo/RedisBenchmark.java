package com.example.demo;

import com.example.demo.dao.redis.JedisHelper;
import com.example.demo.dao.redis.MediaStreamRepository;
import com.example.demo.dao.redis.MediaStreamRepositoryImpl;
import com.example.demo.model.MediaStream;
import org.junit.Before;
import org.junit.Test;
import redis.clients.jedis.JedisPool;

import java.util.Collections;

public class RedisBenchmark extends BaseBenchmark {

    private JedisPool pool = JedisHelper.pool("localhost", 6379);
    private MediaStreamRepository mediaStreamRepository = new MediaStreamRepositoryImpl(pool, 1);

    @Before
    public void init() {
        streams.clear();
        mediaStreamRepository.deleteAll();
    }

    @Test
    public void testSaveMediaStream() {
        measure("saveMediaStream", () -> saveStream(DEPLOYMENT1));
    }

    @Test
    public void testFindMediaStream() {
        saveStreams(1000, DEPLOYMENT1);
        measure("findMediaStream", this::findRandomStream);
    }

    @Test
    public void testFindMediaStreamsByDeploymentId() {
        saveStreams(1000, DEPLOYMENT1);
        saveStreams(10, DEPLOYMENT2);
        measure("getMediaStreamsByDeploymentId", () -> mediaStreamRepository.getMediaStreamsByDeploymentId(DEPLOYMENT2));
    }

    @Test
    public void testDeleteMediaStream() {
        for (int i = 0; i < 3; i++) {
            streams.clear();
            saveStreams(10000, DEPLOYMENT1);
            long start = System.currentTimeMillis();
            for (String id : streams) {
                mediaStreamRepository.deleteMediaStream(id);
            }
            long end = System.currentTimeMillis();
            double op = (end - start) * 1.0 / streams.size();
            System.out.println(String.format("%s iteration #%d %.2f ms", "deleteMediaStream", i, op));
        }
    }

    @Test
    public void testThroughput() {
        findMaxThroughput();
    }

    @Override
    protected MediaStream saveMediaStream(MediaStream ms) {
        mediaStreamRepository.saveMediaStream(ms, Collections.emptySet());
        return ms;
    }

    @Override
    protected MediaStream loadMediaStream(String id) {
        return mediaStreamRepository.findMediaStream(id);
    }
}
